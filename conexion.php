<?php

$host       = "localhost";
$usuario   = "root";
$contraseña   = "admin";
$base_datos     = "database";
$dsn        = "mysql:host=$host;dbname=$base_datos";
// $opciones    = array(
//                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
//              );
try {
  $conexion = new PDO($dsn, $usuario, $contraseña);
  $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  echo"Se ha establecido una conexion con el servidor base de datos";
} catch (PDOException $e) {
  echo "Error en la conexion de base de datos", $e->getMessage();
}
?>
